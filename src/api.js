import axios from "axios";

export default {
  user: {
    login: credentials =>
      axios.post('http://127.0.0.1:5000/login',  credentials ).then(res => res.data.user)
  }
};
