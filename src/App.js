import React from "react";
import { Route } from "react-router-dom";
import HomePage from "./components/pages/HomePage";
import LoginPage from "./components/pages/LoginPage";
import Header from "./components/pages/Header";
import Footer from "./components/pages/Footer";

const App = () => (
  <div className="ui container">
  <Header/>
    <Route path="/" exact component={HomePage} />
    <Route path="/login" exact component={LoginPage} />
  <Footer/>
  </div>
);

export default App;
