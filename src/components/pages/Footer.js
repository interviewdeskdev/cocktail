import React from 'react'
import { Segment } from 'semantic-ui-react'

const Footer = () => (
  <div>
    <Segment textAlign='center'>
      Copyright © 2017 INTERVIEWDESK, INC. All rights reserved.
    </Segment>
  </div>
)

export default Footer
