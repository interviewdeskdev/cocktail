import React from "react";
import { Link } from "react-router-dom";

const HomePage = () => (
  <div>
    <h2> Welcome to InterviewDesk </h2><br/>
    <h3> India's First on demand interviewer platform </h3>
  </div>
);

export default HomePage;
